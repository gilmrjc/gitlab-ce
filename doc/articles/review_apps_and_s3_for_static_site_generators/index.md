# Review Apps and S3 for Static Site Generators

When you develop a website, you need to look for visual changes. Using Review
Apps you can create a temporary site for each feature branch to see how the
modifications look like. This allows web developers and designers to review the
design, which can't be unit tested as backend code can.

## Static Site Generators

If you follow current trends you will know some people state that [Static Site
Generators are the next big
thing](https://www.smashingmagazine.com/2015/11/modern-static-website-generators-next-big-thing/)
and maybe you even have a static blog using Git to version it.  There are
[lots of articles
written](https://about.gitlab.com/2016/06/10/ssg-overview-gitlab-pages-part-2/)
about static sites with GitLab but there are things that you may not know by
now, and we will discuss them.

The first thing you should know is that Amazon S3 service can be used to host
your sites. There's a chance you already use it for other purposes, but if
you're wondering about what AWS S3 is, Amazon Simple Storage Service (Amazon S3)
is a scalable, high-speed, low-cost, web-based cloud storage service. With
this service you will pay for storage, not for a full blown server that you
don't need when working with static pages, with a guarantee of 99.9% uptime from
Amazon. Of course, to use it you need to have an AWS account, something will be
assumed you have through this guide.

Thanks to the AWS API's we can create buckets (where files are placed inside S3)
programmatically and set them to host websites. Each bucket is accessible from
a URL with the following format: `<bucket-name>.<s3-region-specific-endpoint>`.
You can find a list of the AWS Region-specific website endpoints
[here](http://docs.aws.amazon.com/general/latest/gr/rande.html#s3_website_region_endpoints).
To use a custom domain all you have to do is create a DNS CNAME record pointing
to the bucket, and give the bucket the name of the record. Making this you can
access your website from `www.mydomain.com.<s3-region-specific-endpoint>` and
`www.mydomain.com`.

**Note: If you want to use a root domain, which usually uses an A record, you
will need a DNS provider with CNAME flattening, like CloudFlare or
CloudFront.**

Now that you have hosting, you need the site. It's assumed through this guide
you use a SSG to develop it. In a typical workflow the web developers run
a local server, work on it and then push their changes to GitLab. If someone
else wants to see the modifications, it's necesary to pull the branches, run
a local server and give feedback. Every change in the code must follow the same
process. How does a change is shown to a client? You can't ask them to pull the
branch and run the local server. The best way would be to generate a temporary
site and give the link to them. Also it's easier to share a URL with a designer
or any non-technical collaborator and ask for feedback. There's no need to open
up a terminal when they can view it on the browser right away. 

## Review Apps

Review Apps is a GitLab feature that allows you to create temporary deployments
to test branch features. You can find an interesting post about [deployments and
environments](https://about.gitlab.com/2016/08/26/ci-deployment-and-environments/)
where the example used is an imaginary news portal hosted on GitLab.com, similar
to any static site project you can work on, the main difference being the
example is just a set of HTML files instead of a SSG. The example doesn't use an
SSG because Review Apps implementation depends on your technology stack and on
your deployment process, which is what will cover here.

To make a simple example we will create a new site. I'll use
[Middleman](https://middlemanapp.com), but you are free to use any SSG of your
choice. There are [many options](https://www.staticgen.com/) to explore and
GitLab has [several examples](https://about.gitlab.com/features/pages/) to show
you how to setup the CI to build the sites. The Review Apps setup will be
covered in this guide.

In this example, we will work in a new design for a site. We want to show the
changes to our colleagues without deploying to production. This is where Review
Apps come to play. In this case the S3 buckets are created and deleted
automatically as part of the review apps workflow.

## Configuring the CI

To make Review Apps work we need 3 things: the enviroment variables,
a deployment script and a custom `.gitlab-ci.yml`.

The following secret variables are needed to use S3:

- AWS\_ACCESS\_KEY\_ID
- AWS\_DEFAULT\_REGION
- AWS\_SECRET\_ACCESS\_KEY

The following deployment script can be copy-paste as `deploy.py` in your root
project directory. Make sure the script is executable with `chmod +x deploy.py`.
This script creates and deletes the buckets for you. What it does is look for an
env variable named `BUCKET_NAME` to create the bucket, and uploads each file
in `STATIC_DIR`. If you don't specify a `STATIC_DIR` variable, the files inside
the directory `public` will be uploaded by default, because this is the same
directory GitLab Pages uses.


```python
#!/usr/bin/env python3

import os
import argparse
import mimetypes

import boto3

parser = argparse.ArgumentParser()
parser.add_argument('-d', '--delete', action='store_true')
args = parser.parse_args()

bucket_name = os.environ.get('BUCKET_NAME').lower()

static_dir = os.environ.get('STATIC_DIR', 'public')
# Make space for the final slash (dir/)
static_dir_len = len(static_dir) + 1

s3 = boto3.resource('s3')
client = boto3.client('s3')


def delete_bucket():
    try:
        bucket = s3.Bucket(bucket_name)
        for page in bucket.objects.pages():
            objects_to_delete = []
            for obj in page:
                objects_to_delete.append({'Key': obj.key})
            bucket.delete_objects(Delete={'Objects': objects_to_delete})
        bucket.delete()
    except client.exceptions.NoSuchBucket:
        pass

def deploy_bucket():
   print("Deploying %s" % bucket_name)

   if not s3.Bucket(bucket_name) in s3.buckets.all():
       client.create_bucket(Bucket=bucket_name)

   response = client.put_bucket_website(
       Bucket=bucket_name,
       WebsiteConfiguration={
           'ErrorDocument': {
               'Key': '404.html'
           },
           'IndexDocument': {
               'Suffix': 'index.html'
           },
       }
   )

   for root, _, filenames in os.walk(static_dir):
       for filename in filenames:
           file = os.path.join(root, filename)
           type = mimetypes.guess_type(file)[0] or ''
           options = {}
           options['ACL'] = 'public-read'
           options['ContentType'] = type
           client.upload_file(
               file,
               bucket_name,
               file[static_dir_len:],
               options
           )
           print("Uploaded %s" % file)



if args.delete:
   delete_bucket()
else:
   deploy_bucket()

```

The `.gitlab-ci.yml` file will contain the following rules:

```yaml
image: python:3-alpine

stages:
 - build
 - review
 - staging
 - production

build:
 stage: build
 image: ruby:2.3
 cache:
   paths:
     - vendor
     - public
 before_script:
   - apt-get update >/dev/null
   - apt-get install -y nodejs
   - bundle install --path vendor
 script:
   - bundle exec middleman build
 artifacts:
   paths:
     - public
   expire_in: 1 week

review:
 stage: review
 variables:
   BUCKET_NAME: $CI_COMMIT_REF_NAME-$CI_PROJECT_NAME-$CI_PROJECT_NAMESPACE
   ENDPOINT: s3-website-us-east-1.amazonaws.com
 before_script:
   - pip install boto3
 script:
   - ./deploy.py
 environment:
   name: review/$CI_COMMIT_REF_NAME
   url: http://$CI_COMMIT_REF_NAME-$CI_PROJECT_NAME-$CI_PROJECT_NAMESPACE.$ENDPOINT
   on_stop: stop_review
 only:
   - branches
 except:
   - master

stop_review:
 stage: review
 variables:
   BUCKET_NAME: $CI_COMMIT_REF_NAME-$CI_PROJECT_NAME-$CI_PROJECT_NAMESPACE
 before_script:
   - pip install boto3
 script:
   - ./deploy.py -d
 when: manual
 environment:
   name: review/$CI_COMMIT_REF_NAME
   action: stop
 only:
   - branches
 except:
   - master

staging:
 stage: staging
 variables:
   BUCKET_NAME: "stage.mydomain.com"
 before_script:
   - pip install boto3
 script:
   - ./deploy.py
 environment:
   name: staging
   url: http://$BUCKET_NAME
 only:
   - master

production-www:
 stage: production
 variables:
   BUCKET_NAME: "www.mydomain.com"
 before_script:
   - pip install boto3
 script:
 - ./deploy.py
 when: manual
 environment:
   name: production/www-domain
   url: http://$BUCKET_NAME
 only:
   - master
```

What this file does to leverage the Review Apps is the following:

1. Define the needed variables. In this case each bucket name will use the
   format `<branch-name>-<project-name>-<project-owner>`

  ```yaml
  review:
   stage: review
   variables:
     BUCKET_NAME: $CI_COMMIT_REF_NAME-$CI_PROJECT_NAME-$CI_PROJECT_NAMESPACE
     ENDPOINT: s3-website-us-east-1.amazonaws.com
  ```

1. Run the deployment script and set the deployment enviroment with a name and URL

  ```yaml
   before_script:
     - pip install boto3
   script:
     - ./deploy.py
   environment:
     name: review/$CI_COMMIT_REF_NAME
     url: http://$CI_COMMIT_REF_NAME-$CI_PROJECT_NAME-$CI_PROJECT_NAMESPACE.$ENDPOINT
     on_stop: stop_review

  ```

1. Set which branches will be reviewed

  ```yaml
   only:
     - branches
   except:
     - master
  ```

1. Define the stop_review job. The only difference with the review job is this
   will delete the bucket and the deploy environment

  ```yaml
  stop_review:
   stage: review
   variables:
     BUCKET_NAME: $CI_COMMIT_REF_NAME-$CI_PROJECT_NAME-$CI_PROJECT_NAMESPACE
   before_script:
     - pip install boto3
   script:
     - ./deploy.py -d
   when: manual
   environment:
     name: review/$CI_COMMIT_REF_NAME
     action: stop
   only:
     - branches
   except:
     - master
  ```

1. Stage the changes when a merge request is accepted. This will use `stage.mydomain.com`

  ```yaml
  staging:
   stage: staging
   variables:
     BUCKET_NAME: "stage.mydomain.com"
   before_script:
     - pip install boto3
   script:
     - ./deploy.py
   environment:
     name: staging
     url: http://$BUCKET_NAME
   only:
     - master
  ```

1. Deployments to production are manual.

  ```yaml
  production-www:
   stage: production
   variables:
     BUCKET_NAME: "www.mydomain.com"
   before_script:
     - pip install boto3
   script:
   - ./deploy.py
   when: manual
   environment:
     name: production/www-domain
     url: http://$BUCKET_NAME
   only:
     - master
  ```

## Navigate to review sites

Inside your new `.gitlab-ci.yml` file there are two variables that have
a straight meaning. During each review job the runner set the following
variables `BUCKET_NAME` and `ENDPOINT`. Remember to set the `ENDPOINT` variable
according to the S3 region you are deploying to. The CI creates a deployment
environment with `name` and `url` generated for the current branch using these
variables. You can see your environments and access them from GitLab.

![Environments](img/environments.png)


When you create a Merge Request [like
this](https://gitlab.com/pages/middleman/merge_requests/6), the merge request
contains a link to the temporary site.

![Merge Request](img/merge-request.png)

## Custom build job

The build job wasn't explained in the previous section. This is because the
build process dependes on the SSG used. This is where the [examples provided by
GitLab](https://gitlab.com/groups/pages) come to the rescue. You can check them
to see how you to configure your CI for this. From the sample projects you need
to adapt the build job, for example the nanoc project has the following
`.gitlab-ci.yml`:

```yaml
image: ruby:2.3

pages:
  script:
  - bundle install -j4
  - nanoc
  artifacts:
    paths:
    - public
  only:
  - master
```

This will be converted to:

```yaml
build:
  stage: build
  image: ruby:2.3
  script:
    - bundle install -j4
    - nanoc
  artifacts:
    paths:
      - public
    expire_in: 1 week
```

The metalsmith example has this `.gitlab-ci.yml`

```yaml
image: node:4.2.2

pages:
  cache:
    paths:
    - node_modules/

  script:
    - npm install -g metalsmith
    - npm install
    - make build
  artifacts:
    paths:
      - public
  only:
  - master
```

That will be converted to:

```yaml
build:
  stage: build
  image: node:4.2.2
  cache:
    paths:
      - node_modules/
  script:
    - npm install -g metalsmith
    - npm install
    - make build
  artifacts:
    paths:
     - public
   expire_in: 1 week
```

## Continuous deployment

Note that this configuration deploys the master branch to staging and make
production deployments manual. This allows you to preview all the combined
changes from feature branches before deploying to production. Think of this as
a safe net.

If you follow a git flow with a `development` branch, you can make the following
modifications:

1. Stage `development`

  ```yaml
  review:
    stage: review
    ...
    except:
      - development
      - master
  
  stop_review:
    stage: review
    ...
    except:
      - development
      - master

  staging:
   stage: staging
   variables:
     BUCKET_NAME: "stage.mydomain.com"
   before_script:
     - pip install boto3
   script:
     - ./deploy.py
   environment:
     name: staging
     url: http://$BUCKET_NAME
   only:
     - development
  ```

2. Deploy `master` to production automatically

  ```
  production-www:
   stage: production
   variables:
     BUCKET_NAME: "www.mydomain.com"
   before_script:
     - pip install boto3
   script:
   - ./deploy.py
   environment:
     name: production/www-domain
     url: http://$BUCKET_NAME
   only:
     - master
  
  ```

At this point you can work in feature branches and see them live in a browser,
no matter what SSG you're using, thanks to GitLab and S3. There's more things
you can do like linting your code, testing you Javascript code, testing for
visual regressions and even creating custom DNS records for each review
environment, but for now the biggest problem is resolved. You can see and share
your progress every time you push code to GitLab.
